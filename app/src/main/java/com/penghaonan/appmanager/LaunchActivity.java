package com.penghaonan.appmanager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.penghaonan.appframework.base.BaseFrameworkActivity;
import com.penghaonan.appmanager.manager.AppManager;
import com.penghaonan.appmanager.t9.T9PanelActivity;

public class LaunchActivity extends BaseFrameworkActivity implements AppManager.ScanAppListener {
    private ProgressBar mProgressBar;
    private TextView mProgressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        mProgressBar = findViewById(R.id.pb_progress);
        mProgressBar.setMax(100);
        mProgressTextView = findViewById(R.id.tv_progress);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppManager.getInstance(getApplicationContext()).scanApps(this);
    }

    @Override
    public void onProgress(int progress, int total) {
        mProgressBar.setMax(total);
        mProgressBar.setProgress(progress);
    }

    @Override
    public void onFinish() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                finish();
                startActivity(new Intent(LaunchActivity.this, T9PanelActivity.class));
            }
        }.start();
    }

    @Override
    public void onProgressShow(int msg) {
        mProgressTextView.setText(msg);
    }
}
