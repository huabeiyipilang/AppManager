package com.penghaonan.appmanager;

import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appmanager.manager.AppManager;
import com.penghaonan.appmanager.t9.AppEntranceManager;

public class DataLoader {
    private static boolean loaded;
    private static boolean loading;

    private static boolean loadTaskCache;

    public static final String ACTION_DATA_PREPARED = "ACTION_DATA_PREPARED";

    public static boolean isLoaded() {
        return loaded;
    }

    public static void startLoad() {
        if (loading) {
            loadTaskCache = true;
            return;
        }
        loading = true;
        AppManager.getInstance().scanApps(new AppManager.ScanAppListener() {
            @Override
            public void onProgress(int progress, int total) {

            }

            @Override
            public void onFinish() {
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        AppEntranceManager.startLoad();
                        loaded = true;
                        Intent intent = new Intent(ACTION_DATA_PREPARED);
                        LocalBroadcastManager.getInstance(AppDelegate.getApp()).sendBroadcast(intent);
                        loading = false;
                        if (loadTaskCache) {
                            loadTaskCache = false;
                            startLoad();
                        }
                    }
                }.start();
            }

            @Override
            public void onProgressShow(int msg) {

            }
        });
    }
}
