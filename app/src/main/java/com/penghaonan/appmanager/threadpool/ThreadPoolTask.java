package com.penghaonan.appmanager.threadpool;

/**
 * Created by carl on 16/3/30.
 */
abstract public class ThreadPoolTask implements Runnable {

    private Object key;
    private ThreadPoolManager manager;

    public void setKey(Object keyObj){
        key = keyObj;
    }

    void setManager(ThreadPoolManager mgr){
        manager = mgr;
    }

    /**
     * 返回标识
     */
    public Object getKey(){
        return key == null ? this : key;
    }

    @Override
    public final void run() {
        if (!Thread.currentThread().isInterrupted()){
            doInBackground();
        }
        if (manager != null){
            manager.destroyTask(this);
        }
    }

    protected abstract void doInBackground();
}
