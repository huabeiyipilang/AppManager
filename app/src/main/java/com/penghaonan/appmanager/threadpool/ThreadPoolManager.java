package com.penghaonan.appmanager.threadpool;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by carl on 16/3/30.
 */
public class ThreadPoolManager {
    private final static String TAG = "ThreadPoolManager";
    private ExecutorService mExecutorService;
    private Queue<ThreadPoolTask> mTaskQueue;
    private Map<Object, Future> mFutrueMap;
    private Thread mPoolThread;
    public ThreadPoolManager(int size){
        mExecutorService = Executors.newFixedThreadPool(size);
        mTaskQueue = new ConcurrentLinkedQueue<>();
        mFutrueMap = new ConcurrentHashMap<>();
    }

    public void start() {
        if (mPoolThread == null) {
            mPoolThread = new Thread(new PoolRunnable());
            mPoolThread.start();
        }
    }

    /**
     * 结束轮询，关闭线程池
     */
    public void stop() {
        mPoolThread.interrupt();
        mPoolThread = null;
    }

    public void addTask(ThreadPoolTask task){
        cancelTask(task);
        mTaskQueue.add(task);
    }

    public ThreadPoolTask pollTask(){
        return mTaskQueue.poll();
    }

    public void cancelTask(ThreadPoolTask task){
        boolean removed = mTaskQueue.remove(task);
        if (!removed){
            Future future = mFutrueMap.get(task.getKey());
            if (future != null){
                future.cancel(true);
            }
        }
    }

    public void cancelTask(Object key){
        for (ThreadPoolTask task : mTaskQueue){
            if (task.getKey() == key){
                boolean removed = mTaskQueue.remove(task);
                if (removed){
                    return;
                }else {
                    break;
                }
            }
        }
        Future future = mFutrueMap.get(key);
        if (future != null){
            future.cancel(true);
        }
    }

    void destroyTask(ThreadPoolTask task){
        mFutrueMap.remove(task.getKey());
    }

    private class PoolRunnable implements Runnable {

        @Override
        public void run() {
            try{
                while (!Thread.currentThread().isInterrupted()){
                    ThreadPoolTask task = pollTask();
                    if (task == null){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ignore) {
                        }
                        continue;
                    }
                    Future<?> future = mExecutorService.submit(task);
                    mFutrueMap.put(task.getKey(), future);
                }
            }finally {
                mExecutorService.shutdown();
            }
        }
    }
}
