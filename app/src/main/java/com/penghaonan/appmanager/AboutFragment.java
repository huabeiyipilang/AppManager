package com.penghaonan.appmanager;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.penghaonan.appmanager.base.BaseFragment;

public class AboutFragment extends BaseFragment {
    private static final String TAG = "AboutFragment";
    @Override
    public int getLayoutRes() {
        return R.layout.fragment_about;
    }

    @Override
    public void initViews(View root) {
        Context context = App.getApp();
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
            ((TextView)findViewById(R.id.tv_version)).setText(packInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        WebView webView = (WebView) findViewById(R.id.wv_html);
        webView.setBackgroundColor(0);
        webView.loadUrl("file:///android_asset/about.html");
    }

    @Override
    public void initDatas() {

    }

    @Override
    public String getFragmentName() {
        return TAG;
    }
}
