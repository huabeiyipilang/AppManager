package com.penghaonan.appmanager.list;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.penghaonan.appmanager.AboutFragment;
import com.penghaonan.appmanager.App;
import com.penghaonan.appmanager.BlankActivity;
import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.SearchActivity;
import com.penghaonan.appmanager.base.BaseFragment;
import com.penghaonan.appmanager.base.ItemAdapter;
import com.penghaonan.appmanager.detail.DetailFragment;
import com.penghaonan.appmanager.manager.AppInfo;
import com.penghaonan.appmanager.manager.AppManager;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class AppListFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private static final String TAG = "AppListFragment";

    private ItemAdapter<AppInfo> mAdapter;
    private ListView mListView;
    private AppManager mAppManager;
    private SummaryInfo[] mSummaryInfos = {
            new SummaryInfo(SummaryInfo.SUMMARY_INFO_PKG_NAME, R.string.package_name),
            new SummaryInfo(SummaryInfo.SUMMARY_INFO_VERSION, R.string.version_name)
    };
    private AlertDialog mSummarySelectDialog;

    //status
    private boolean mShowSystemApp;
    private SummaryInfo mSummaryInfo = mSummaryInfos[0];

    public AppListFragment() {
        mAppManager = AppManager.getInstance(App.getApp());
    }

    public static AppListFragment newInstance() {
        return new AppListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.release();
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_app_list;
    }

    @Override
    public void initViews(View root) {
        mListView = (ListView) findViewById(R.id.listView);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void initDatas() {
        AppItemView.mSummaryInfo = mSummaryInfo;
        mAdapter = new ItemAdapter<>(null, AppItemView.class);
        mListView.setAdapter(mAdapter);
        updateApps(mShowSystemApp, null);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    /**
     * @param showSystemApp 显示系统应用
     * @param keyword       null:不过滤关键词  "":不匹配任何关键词
     */
    public void updateApps(boolean showSystemApp, String keyword) {
        List<AppInfo> infoList = null;
        if (!"".equals(keyword)) {
            infoList = mAppManager.getAppList(showSystemApp);
            if (keyword != null) {
                Iterator<AppInfo> infoIterator = infoList.iterator();
                while (infoIterator.hasNext()) {
                    AppInfo appInfo = infoIterator.next();
                    // T9 搜索
                    String t9 = appInfo.getAppNameT9();
                    if (TextUtils.isEmpty(t9) || !t9.contains(keyword)) {
                        infoIterator.remove();
                    }

                    // 名称搜索
//                    String appName = appInfo.getAppName();
//                    if (appName == null || !appName.toLowerCase().contains(keyword.toLowerCase())) {
//                        infoIterator.remove();
//                    }
                }
            }
        }
        mAdapter.setData(infoList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mAdapter.isCheckMode()) {
            mAdapter.toggle(position);
        } else {
            AppInfo appInfo = mAdapter.getItem(position);
            DetailFragment fragment = DetailFragment.newInstance(appInfo);
            BlankActivity.startFragment(fragment, true);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.app_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_select_system_app).setChecked(mShowSystemApp);
        MenuItem item = menu.findItem(R.id.action_summary);
        item.setTitle(getString(R.string.app_list_menu_app_info) + getString(mSummaryInfo.titleRes));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_select_system_app:
                mShowSystemApp = !mShowSystemApp;
                updateApps(mShowSystemApp, null);
                return true;
            case R.id.action_summary:
                showSummarySelectView();
                return true;
            case R.id.action_state_check:
                enterCheckMode();
                return true;
            case R.id.action_about:
                AboutFragment fragment = new AboutFragment();
                BlankActivity.startFragment(fragment, true);
                return true;
            case R.id.action_search:
                startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected boolean onBackKeyDown() {
        if (mAdapter.isCheckMode()) {
            exitCheckMode();
            return true;
        }
        return super.onBackKeyDown();
    }

    private void changeSummary(SummaryInfo summaryInfo) {
        mSummaryInfo = summaryInfo;
        AppItemView.mSummaryInfo = summaryInfo;
        mAdapter.notifyDataSetChanged();
    }

    private void enterCheckMode() {
        if (mAdapter.isCheckMode()) {
            return;
        }
        mAdapter.setCheckMode(true);
    }

    private void exitCheckMode() {
        if (mAdapter.isCheckMode()) {
            mAdapter.setCheckMode(false);
        }
    }

    private void showSummarySelectView() {
        if (mSummarySelectDialog == null) {
            ItemAdapter<SummaryInfo> adapter = new ItemAdapter<>();
            adapter.setView(SummaryInfoSelectView.class);
            adapter.setData(Arrays.asList(mSummaryInfos));
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    changeSummary(mSummaryInfos[which]);
                }
            });
            mSummarySelectDialog = builder.create();
        }
        if (!mSummarySelectDialog.isShowing()) {
            mSummarySelectDialog.show();
        }
    }

    public static class SummaryInfo {
        public final static int SUMMARY_INFO_PKG_NAME = 1;
        public final static int SUMMARY_INFO_VERSION = 2;
        public int id;
        public int titleRes;

        SummaryInfo(int id, int titleRes) {
            this.id = id;
            this.titleRes = titleRes;
        }
    }
}
