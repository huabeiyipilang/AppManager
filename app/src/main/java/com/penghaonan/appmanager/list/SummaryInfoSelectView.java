package com.penghaonan.appmanager.list;

import android.content.Context;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseItemView;

public class SummaryInfoSelectView extends BaseItemView {
    private TextView mTitleView;

    public SummaryInfoSelectView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.view_context_menu_item;
    }

    @Override
    protected void initViews() {
        mTitleView = findViewById(R.id.tv_title);
    }

    @Override
    public boolean bindData(Object data) {
        AppListFragment.SummaryInfo info = (AppListFragment.SummaryInfo) data;
        mTitleView.setText(info.titleRes);
        return false;
    }
}
