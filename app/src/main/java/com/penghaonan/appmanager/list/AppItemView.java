package com.penghaonan.appmanager.list;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseItemView;
import com.penghaonan.appmanager.manager.AppInfo;


public class AppItemView extends BaseItemView {

    private ImageView mIconView;
    private TextView mNameView;
    private TextView mSummaryView;
    private CheckBox mCheckView;

    public static AppListFragment.SummaryInfo mSummaryInfo;

    public AppItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.view_app_list_item;
    }

    @Override
    protected void initViews() {
        mIconView = findViewById(R.id.iv_icon);
        mNameView = findViewById(R.id.tv_name);
        mSummaryView = findViewById(R.id.tv_pkg_name);
        mCheckView = findViewById(R.id.cb_checked);
    }

    @Override
    protected void setCheckable(boolean checkable) {
        mCheckView.setVisibility(checkable ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected void setChecked(boolean checked) {
        mCheckView.setChecked(checked);
    }

    @Override
    public boolean bindData(Object data) {
        AppInfo info = (AppInfo) data;
        mNameView.setText(info.getAppName());
        switch (mSummaryInfo.id) {
            case AppListFragment.SummaryInfo.SUMMARY_INFO_PKG_NAME:
                mSummaryView.setText(info.getPackageName());
                break;
            case AppListFragment.SummaryInfo.SUMMARY_INFO_VERSION:
                mSummaryView.setText(getContext().getString(R.string.app_list_summary_version, info.pkgInfo.versionName, info.pkgInfo.versionCode));
                break;
        }
        Drawable icon = info.getIconCache();
        if (icon == null) {
            return true;
        } else {
            mIconView.setImageDrawable(icon);
            return false;
        }
    }

    @Override
    public void asyncBindData(Object data) {
        AppInfo info = (AppInfo) data;
        final Drawable drawable = info.getIcon();
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        post(new Runnable() {
            @Override
            public void run() {
                mIconView.setImageDrawable(drawable);
            }
        });

    }
}
