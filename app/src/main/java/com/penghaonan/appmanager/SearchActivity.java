package com.penghaonan.appmanager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.penghaonan.appmanager.base.BaseActivity;
import com.penghaonan.appmanager.list.AppListFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity {

    private AppListFragment mAppListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        mAppListFragment = (AppListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_app_list);
        mAppListFragment.updateApps(true, "");
        EditText editText = findViewById(R.id.et_input);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAppListFragment.updateApps(true, charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.bt_close)
    void onCloseClick() {
        finish();
    }

}
