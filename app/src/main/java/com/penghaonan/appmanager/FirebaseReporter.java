package com.penghaonan.appmanager;

import android.app.Activity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.base.BaseFrameworkFragment;
import com.penghaonan.appframework.reporter.IReporter;

import java.util.Map;

import androidx.annotation.NonNull;

public class FirebaseReporter implements IReporter {

    private FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(AppDelegate.getApp());

    @Override
    public void setChannel(String channel) {
        mFirebaseAnalytics.setUserProperty("channel", channel);
    }

    @Override
    public void onActivityResume(@NonNull Activity activity) {
        mFirebaseAnalytics.setCurrentScreen(activity, activity.getClass().getSimpleName(), null);
    }

    @Override
    public void onActivityPause(@NonNull Activity activity) {

    }

    @Override
    public void onFragmentResume(@NonNull BaseFrameworkFragment fragment) {
        Activity activity = fragment.getActivity();
        if (activity == null) {
            return;
        }
        mFirebaseAnalytics.setCurrentScreen(activity, fragment.getClass().getSimpleName(), null);
    }

    @Override
    public void onFragmentPause(@NonNull BaseFrameworkFragment fragment) {

    }

    @Override
    public void onEvent(String eventId) {

    }

    @Override
    public void onEvent(String eventId, String value) {

    }

    @Override
    public void onEvent(String eventId, Map<String, String> values) {

    }
}
