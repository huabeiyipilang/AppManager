package com.penghaonan.appmanager.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.penghaonan.appframework.base.BaseFrameworkActivity;
import com.penghaonan.appframework.utils.UIUtils;
import com.penghaonan.appmanager.R;

import androidx.annotation.Nullable;

public class BaseTransparentActivity extends BaseActivity {
    private ViewGroup rootTransView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtils.initActivityStatusNavigationBarColor(this, getResources().getColor(R.color.t9_panel_bkg), getResources().getColor(R.color.t9_panel_bkg));
        super.setContentView(R.layout.activity_base_transparent);
        rootTransView = findViewById(R.id.view_trans_root);
        rootTransView.setOnClickListener(v -> finish());
    }

    @Override
    public void setContentView(int layoutResID) {
        View view = getLayoutInflater().inflate(layoutResID, null);
        rootTransView.removeAllViews();
        rootTransView.addView(view);
    }

    @Override
    public void setContentView(View view) {
        rootTransView.removeAllViews();
        rootTransView.addView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        rootTransView.removeAllViews();
        rootTransView.addView(view, params);
    }
}