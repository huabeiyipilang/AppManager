package com.penghaonan.appmanager.base;

import android.content.Context;
import android.widget.RelativeLayout;

abstract public class BaseItemView<T> extends RelativeLayout {

    public BaseItemView(Context context) {
        super(context);
        inflate(context, getLayoutRes(), this);
        initViews();
    }

    abstract protected int getLayoutRes();

    abstract protected void initViews();

    /**
     * 绑定数据
     * @return true:启用异步绑定,在asyncBindData中完成   false:不启用异步绑定
     */
    abstract public boolean bindData(T data);

    /**
     * 异步加载数据
     */
    protected void asyncBindData(T data){
    }

    /**
     * 是否多选
     */
    protected void setCheckable(boolean checkable){
    }

    /**
     * 是否被选中
     */
    protected void setChecked(boolean checked){
    }
}
