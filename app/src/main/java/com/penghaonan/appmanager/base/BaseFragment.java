package com.penghaonan.appmanager.base;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.penghaonan.appframework.base.BaseFrameworkFragment;

abstract public class BaseFragment extends BaseFrameworkFragment {
    private View mRootView;
    private ActionBar mActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutRes(), container, false);
        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        initViews(mRootView);
        initDatas();
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    protected View findViewById(int id) {
        if (mRootView == null) {
            return null;
        }
        return mRootView.findViewById(id);
    }

    protected ActionBar getActionBar() {
        return mActionBar;
    }

    /**
     * 接收返回键按下事件
     *
     * @return boolean  false:back键事件未处理，向下传递。  true：消费掉该事件。
     * @Title: onBackKeyDown
     * @date 2014-3-10 上午11:15:33
     */
    protected boolean onBackKeyDown() {
        return false;
    }

    abstract public int getLayoutRes();

    abstract public void initViews(View root);

    abstract public void initDatas();
}
