package com.penghaonan.appmanager.base;

import android.os.Bundle;

import com.penghaonan.appframework.base.BaseFrameworkActivity;
import com.penghaonan.appframework.utils.UIUtils;
import com.penghaonan.appmanager.R;

import androidx.annotation.Nullable;

public class BaseActivity extends BaseFrameworkActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtils.initActivityStatusNavigationBarColor(this, R.color.colorPrimary, R.color.colorPrimary);
    }
}