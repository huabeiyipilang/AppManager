package com.penghaonan.appmanager.base;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.penghaonan.appframework.utils.CollectionUtils;
import com.penghaonan.appmanager.App;
import com.penghaonan.appmanager.threadpool.ThreadPoolTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ItemAdapter<T> extends BaseAdapter {

    private List<T> mDatas = new ArrayList<>();
    private Class<? extends BaseItemView> mViewClass;
    private Context mContext = App.getApp();
    private boolean mCheckMode;
    private List<Integer> mCheckedIndexs;
    private ExecutorService mThreadExecutor;

    public ItemAdapter() {
        init();
    }

    public ItemAdapter(List<T> list, Class<? extends BaseItemView> viewClass) {
        setData(list);
        mViewClass = viewClass;
        init();
    }

    private void init() {
    }

    public void setData(List<T> list) {
        mDatas.clear();
        mDatas.addAll(list);
    }

    public void setView(Class<? extends BaseItemView> viewClass) {
        mViewClass = viewClass;
    }

    @Override
    public int getCount() {
        return CollectionUtils.size(mDatas);
    }

    @Override
    public T getItem(int arg0) {
        return mDatas == null || mDatas.size() == 0 ? null : mDatas.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int index, View convertView, ViewGroup arg2) {
        BaseItemView view;
        if (convertView == null) {
            try {
                view = mViewClass.getConstructor(Context.class)
                        .newInstance(mContext);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            view = (BaseItemView) convertView;
        }
        T data = getItem(index);
        view.setCheckable(mCheckMode);
        if (mCheckMode) {
            view.setChecked(isChecked(index));
        }
        boolean needAsync = view.bindData(data);
        if (needAsync) {
            getExecutorService().execute(new BindTask(view, data));
        }
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    /**
     * 多行模式
     */
    public void setCheckMode(boolean checkMode) {
        mCheckMode = checkMode;
        if (mCheckMode) {
            mCheckedIndexs = new ArrayList<>();
        } else {
            mCheckedIndexs = null;
        }
        notifyDataSetChanged();
    }

    /**
     * 是否是多行模式
     */
    public boolean isCheckMode() {
        return mCheckMode;
    }

    /**
     * 是否已选中
     */
    public boolean isChecked(int index) {
        return mCheckedIndexs.contains(index);
    }

    /**
     * 切换选中状态
     */
    public void toggle(int index) {
        if (mCheckedIndexs.contains(index)) {
            mCheckedIndexs.remove((Integer) index);
        } else {
            mCheckedIndexs.add(index);
        }
        notifyDataSetChanged();
    }

    public void release() {
        if (mThreadExecutor != null) {
            mThreadExecutor.shutdown();
            mThreadExecutor = null;
        }
    }

    private ExecutorService getExecutorService() {
        if (mThreadExecutor == null) {
            synchronized (ItemAdapter.class) {
                if (mThreadExecutor == null) {
                    mThreadExecutor = Executors.newSingleThreadExecutor();
                }
            }
        }
        return mThreadExecutor;
    }

    private class BindTask extends ThreadPoolTask {
        private BaseItemView view;
        private T data;

        BindTask(BaseItemView view, T data) {
            this.view = view;
            this.data = data;
            setKey(view);
        }

        @Override
        protected void doInBackground() {
            view.asyncBindData(data);
        }
    }
}
