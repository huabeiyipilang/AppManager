package com.penghaonan.appmanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.utils.Logger;

public class AppReceiver extends BroadcastReceiver {

    public static void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
            intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
            intentFilter.addDataScheme("package");
        }
        AppDelegate.getApp().registerReceiver(new AppReceiver(), intentFilter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        String action = intent.getAction();
        if (action == null) {
            return;
        }
        Logger.i("onReceive action : " + action);
        switch (action) {
            case Intent.ACTION_PACKAGE_ADDED:
            case Intent.ACTION_PACKAGE_CHANGED:
            case Intent.ACTION_PACKAGE_REMOVED:
                DataLoader.startLoad();
                break;
        }
    }
}
