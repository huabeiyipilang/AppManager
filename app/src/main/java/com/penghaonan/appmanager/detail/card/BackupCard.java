package com.penghaonan.appmanager.detail.card;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.utils.ToastUtils;
import com.penghaonan.appmanager.App;
import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.manager.AppInfo;
import com.penghaonan.appmanager.manager.BackupManager;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by carl on 16/3/25.
 */
public class BackupCard extends ARadiusCard implements View.OnClickListener {

    private TextView mInfoView;
    private View mInstallView, mDeleteView, mUpdateView, mBackupView;
    private BackupManager.BackupInfo mBackupInfo;
    private ProgressDialog mProgressDialog;

    public BackupCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);
        mInfoView = findViewById(R.id.tv_info);
        mInstallView = findViewById(R.id.bt_install);
        mInstallView.setOnClickListener(this);
        mDeleteView = findViewById(R.id.bt_delete);
        mDeleteView.setOnClickListener(this);
        mUpdateView = findViewById(R.id.bt_update);
        mUpdateView.setOnClickListener(this);
        mBackupView = findViewById(R.id.bt_backup);
        mBackupView.setOnClickListener(this);
        updateUI();
    }

    @Override
    protected int layout() {
        return R.layout.card_backup;
    }

    private void updateUI(){
        mBackupInfo = BackupManager.getBackupInfo(mAppInfo);
        mInfoView.setText(mBackupInfo.getDescription());
        mInstallView.setVisibility(View.GONE);
        mDeleteView.setVisibility(View.GONE);
        mUpdateView.setVisibility(View.GONE);
        mBackupView.setVisibility(View.GONE);
        switch (mBackupInfo.state){
            case BackupManager.BackupInfo.STATE_BACKUP_IS_NEW:
                mDeleteView.setVisibility(View.VISIBLE);
                mInstallView.setVisibility(View.VISIBLE);
                break;
            case BackupManager.BackupInfo.STATE_BACKUPED:
                mDeleteView.setVisibility(View.VISIBLE);
                break;
            case BackupManager.BackupInfo.STATE_HAS_NEW_VERSION:
                mDeleteView.setVisibility(View.VISIBLE);
                mUpdateView.setVisibility(View.VISIBLE);
                break;
            case BackupManager.BackupInfo.STATE_NOT_BACKUP:
                mBackupView.setVisibility(View.VISIBLE);
                break;
        }

        setTitle(getTitle(mBackupInfo.state));
    }

    @Override
    public void onCardClick() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BackupManager.cancelBackup(mAppInfo);
    }

    private int getTitle(int state){
        switch (state){
            case BackupManager.BackupInfo.STATE_BACKUP_IS_NEW:
                return R.string.backup_state_backup_is_new;
            case BackupManager.BackupInfo.STATE_BACKUPED:
                return R.string.backup_state_backuped;
            case BackupManager.BackupInfo.STATE_HAS_NEW_VERSION:
                return R.string.backup_state_has_new_version;
            case BackupManager.BackupInfo.STATE_NOT_BACKUP:
                return R.string.backup_state_not_backup;
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_update:
            case R.id.bt_backup:
                showProgressDialog();
                BackupManager.backupApp(mAppInfo, new BackupManager.BackupListener() {
                    @Override
                    public void onBackupCallback(int code) {
                        ToastUtils.showToast(BackupManager.getCodeDescription(code));
                        hideProgressDialog();
                        AppDelegate.post(new Runnable() {
                            @Override
                            public void run() {
                                updateUI();
                            }
                        });
                    }
                });
                break;
            case R.id.bt_delete:
                if (mBackupInfo != null){
                    mBackupInfo.delete();
                }
                AppDelegate.post(new Runnable() {
                    @Override
                    public void run() {
                        updateUI();
                    }
                });
                break;
            case R.id.bt_install:
                if (mBackupInfo != null && mBackupInfo.file != null && mBackupInfo.file.exists()){
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(Uri.parse("file://" + mBackupInfo.file.getAbsolutePath()), "application/vnd.android.package-archive");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    App.getApp().startActivity(i);
                }
                break;
        }
    }

    private void showProgressDialog(){
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getActivity().getString(R.string.backup_progress_running));
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setOnDismissListener(dialog -> BackupManager.cancelBackup(mAppInfo));
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog(){
        if (mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }
}
