package com.penghaonan.appmanager.detail.card;

import androidx.fragment.app.FragmentActivity;
import android.widget.LinearLayout;

import com.penghaonan.appmanager.manager.AppInfo;

/**
 * Created by likai on 15/11/16.
 */
abstract public class ACard extends LinearLayout {
    protected AppInfo mAppInfo;
    private FragmentActivity mActivity;

    public ACard(FragmentActivity activity, AppInfo appInfo) {
        super(activity);
        mAppInfo = appInfo;
        mActivity = activity;
        initLayout();
    }

    protected void initLayout(){
        inflate(getContext(), layout(), this);
    }

    protected abstract int layout();

    abstract public void onCardClick();

    public void onDestroy(){}

    protected FragmentActivity getActivity(){
        return mActivity;
    }
}
