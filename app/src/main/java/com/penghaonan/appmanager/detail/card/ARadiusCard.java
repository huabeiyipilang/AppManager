package com.penghaonan.appmanager.detail.card;

import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.manager.AppInfo;

/**
 * Created by carl on 12/27/15.
 */
public abstract class ARadiusCard extends ACard {

    protected final static int EXPAND_GONE = 0;
    protected final static int EXPAND_ON = 1;
    protected final static int EXPAND_OFF = 2;


    public ARadiusCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);
    }

    protected void initLayout() {
        inflate(getContext(), R.layout.card_radius_base, this);
        inflate(getContext(), layout(), (ViewGroup) findViewById(R.id.view_card_container));
    }

    protected void setTitle(int titleRes) {
        if (titleRes == 0) {
            findViewById(R.id.view_title_container).setVisibility(View.GONE);
            findViewById(R.id.view_radius_divider).setVisibility(View.GONE);
        } else {
            findViewById(R.id.view_title_container).setVisibility(View.VISIBLE);
            TextView titleView = (TextView) findViewById(R.id.tv_radius_title);
            titleView.setText(titleRes);
            findViewById(R.id.view_radius_divider).setVisibility(View.VISIBLE);
        }
    }

    protected void setExpand(int expand) {
        TextView expandView = (TextView) findViewById(R.id.view_expand);
        switch (expand) {
            case EXPAND_GONE:
                expandView.setVisibility(View.GONE);
                break;
            case EXPAND_ON:
                expandView.setVisibility(View.VISIBLE);
                expandView.setText("-");
                break;
            case EXPAND_OFF:
                expandView.setVisibility(View.VISIBLE);
                expandView.setText("+");
                break;
        }
    }
}
