package com.penghaonan.appmanager.detail.card;

import androidx.fragment.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.manager.AppInfo;

/**
 * Created by baidu on 15/11/19.
 */
public class HeaderCard extends ACard {

    private ImageView mIconView;
    private TextView mNameView;

    public HeaderCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);
        mIconView = (ImageView)findViewById(R.id.iv_icon);
        mNameView = (TextView)findViewById(R.id.tv_title);

        mIconView.setImageDrawable(appInfo.getIcon());
        mNameView.setText(appInfo.getAppName());
    }

    @Override
    protected int layout() {
        return R.layout.card_header;
    }

    @Override
    public void onCardClick() {

    }
}
