package com.penghaonan.appmanager.detail.card;

import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import androidx.fragment.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.detail.card.appsize.AppSizeItemView;
import com.penghaonan.appmanager.manager.AppInfo;
import com.penghaonan.appmanager.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.view.ColumnChartView;

public class PermissionCard extends ARadiusCard {

    private TextView mDetailView;

    public PermissionCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);
        mDetailView = findViewById(R.id.detail_contianer);
        List<PermissionInfo> infos = mAppInfo.getPermissionInfos();
        if (infos == null || infos.size() == 0) {
            findViewById(R.id.tv_empty_view).setVisibility(View.VISIBLE);
            findViewById(R.id.chart_view).setVisibility(View.GONE);
        } else {
            initChart(infos);
        }
    }

    private void initChart(List<PermissionInfo> infos) {
        int dangerous = 0, normal = 0, other = 0;
        for (PermissionInfo permission : infos) {
            if (permission.protectionLevel == PermissionInfo.PROTECTION_DANGEROUS) {
                dangerous++;
            } else if (permission.protectionLevel == PermissionInfo.PROTECTION_NORMAL) {
                normal++;
            } else {
                other++;
            }
        }

        ViewGroup infoContainer = findViewById(R.id.view_info_container);
        ColumnChartView chartView = findViewById(R.id.chart_view);
        chartView.setValueTouchEnabled(false);
        ColumnChartData data = new ColumnChartData();
        List<Column> columns = new ArrayList<>();
        AppSizeItemView itemView;

        if (dangerous != 0) {
            List<SubcolumnValue> values = new ArrayList<>();
            SubcolumnValue value = new SubcolumnValue(dangerous, Constants.CHART_COLOR_RED);
            value.setLabel(getContext().getString(R.string.permission_dangerous));
            values.add(value);
            Column column = new Column(values);
            columns.add(column);
            itemView = new AppSizeItemView(getContext(), Constants.CHART_COLOR_RED,
                    getContext().getString(R.string.permission_dangerous),
                    getResources().getString(R.string.count_unit, dangerous));
            infoContainer.addView(itemView);
        }

        if (normal != 0) {
            List<SubcolumnValue> values = new ArrayList<>();
            SubcolumnValue value = new SubcolumnValue(normal, Constants.CHART_COLOR_GREEN);
            value.setLabel(getContext().getString(R.string.permission_normal));
            values.add(value);
            Column column = new Column(values);
            columns.add(column);
            itemView = new AppSizeItemView(getContext(), Constants.CHART_COLOR_GREEN,
                    getContext().getString(R.string.permission_normal),
                    getResources().getString(R.string.count_unit, normal));
            infoContainer.addView(itemView);
        }

        if (other != 0) {
            List<SubcolumnValue> values = new ArrayList<>();
            SubcolumnValue value = new SubcolumnValue(other, Constants.CHART_COLOR_BLUE);
            value.setLabel(getContext().getString(R.string.permission_other));
            values.add(value);
            Column column = new Column(values);
            columns.add(column);
            itemView = new AppSizeItemView(getContext(), Constants.CHART_COLOR_BLUE,
                    getContext().getString(R.string.permission_other),
                    getResources().getString(R.string.count_unit, other));
            infoContainer.addView(itemView);
        }

        data.setColumns(columns);
        chartView.setColumnChartData(data);

        //标题
        setTitle(R.string.permission);

        //详情
        PackageManager pm = getContext().getPackageManager();
        StringBuilder sb = new StringBuilder();
        for (PermissionInfo permission : infos) {
            CharSequence desc = permission.loadDescription(pm);
            if (TextUtils.isEmpty(desc)) {
                continue;
            }
            sb.append("\n");
            String name = permission.name;
            sb.append(name).append("\n");
            sb.append(desc).append("\n");
        }
        mDetailView.setText(sb.toString());
        setExpand(EXPAND_OFF);
    }

    @Override
    protected int layout() {
        return R.layout.card_permission;
    }

    @Override
    public void onCardClick() {
        if (mDetailView.getVisibility() == View.GONE) {
            mDetailView.setVisibility(View.VISIBLE);
            setExpand(EXPAND_ON);
        } else {
            mDetailView.setVisibility(View.GONE);
            setExpand(EXPAND_OFF);
        }
    }
}
