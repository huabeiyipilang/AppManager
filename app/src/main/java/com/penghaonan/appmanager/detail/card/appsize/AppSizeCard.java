package com.penghaonan.appmanager.detail.card.appsize;

import android.content.pm.PackageStats;
import androidx.fragment.app.FragmentActivity;
import android.view.ViewGroup;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.detail.card.ARadiusCard;
import com.penghaonan.appmanager.manager.AppInfo;
import com.penghaonan.appmanager.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

/**
 * Created by baidu on 15/12/18.
 */
public class AppSizeCard extends ARadiusCard {

    public AppSizeCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);
        PackageStats pkgStats = appInfo.getPkgStats();
        if (pkgStats == null) {
            return;
        }

        ViewGroup infoContainer = (ViewGroup) findViewById(R.id.view_info_container);
        PieChartView chartView = (PieChartView) findViewById(R.id.cv_pie);
        chartView.setValueTouchEnabled(false);
        List<SliceValue> values = new ArrayList<>();
        AppSizeItemView itemView;

        if (pkgStats.codeSize > 0) {
            values.add(new SliceValue(pkgStats.codeSize, Constants.CHART_COLOR_BLUE));
            itemView = new AppSizeItemView(activity, Constants.CHART_COLOR_BLUE,
                    activity.getString(R.string.card_app_size_code_size), pkgStats.codeSize);
            infoContainer.addView(itemView);
        }

        if (pkgStats.dataSize > 0) {
            values.add(new SliceValue(pkgStats.dataSize, Constants.CHART_COLOR_GREEN));
            itemView = new AppSizeItemView(activity, Constants.CHART_COLOR_GREEN,
                    activity.getString(R.string.card_app_size_data_size), pkgStats.dataSize);
            infoContainer.addView(itemView);
        }

        if (pkgStats.cacheSize > 0) {
            values.add(new SliceValue(pkgStats.cacheSize, Constants.CHART_COLOR_RED));
            itemView = new AppSizeItemView(activity, Constants.CHART_COLOR_RED,
                    activity.getString(R.string.card_app_size_cache_size), pkgStats.cacheSize);
            infoContainer.addView(itemView);
        }

        PieChartData data = new PieChartData(values);
        chartView.setPieChartData(data);

        setTitle(R.string.storage_card_title);
    }

    @Override
    protected int layout() {
        return R.layout.card_app_size;
    }

    @Override
    public void onCardClick() {

    }
}
