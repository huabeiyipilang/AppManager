package com.penghaonan.appmanager.detail.card.appsize;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.penghaonan.appmanager.R;

public class AppSizeItemView extends LinearLayout {
    public AppSizeItemView(Context context, int color, String desc, long size) {
        super(context);
        inflate(context, R.layout.item_view_app_size, this);
        findViewById(R.id.view_color).setBackgroundColor(color);
        ((TextView)findViewById(R.id.tv_title)).setText(desc);
        ((TextView)findViewById(R.id.tv_size)).setText(android.text.format.Formatter.formatFileSize(context, size));
    }

    public AppSizeItemView(Context context, int color, String desc, String textRight) {
        super(context);
        inflate(context, R.layout.item_view_app_size, this);
        findViewById(R.id.view_color).setBackgroundColor(color);
        ((TextView)findViewById(R.id.tv_title)).setText(desc);
        ((TextView)findViewById(R.id.tv_size)).setText(textRight);
    }
}
