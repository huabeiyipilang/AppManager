package com.penghaonan.appmanager.detail.card.flag;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.manager.AppInfo;

public class FlagItemView extends LinearLayout {

    private FlagItemView(Context context, String title) {
        super(context);
        inflate(context, R.layout.item_view_flag, this);
        TextView tv = findViewById(R.id.tv_title);
        tv.setText(title);
    }

    public FlagItemView(Context context, String title, AppInfo info, int flag) {
        this(context, title);
        TextView tv = findViewById(R.id.tv_value);
        boolean value = (info.pkgInfo.applicationInfo.flags & flag) != 0;
        tv.setText(value ? context.getString(R.string.app_flag_true) : context.getString(R.string.app_flag_false));
    }

    public FlagItemView(Context context, int title, AppInfo info, int flag) {
        this(context, context.getString(title), info, flag);
    }
}
