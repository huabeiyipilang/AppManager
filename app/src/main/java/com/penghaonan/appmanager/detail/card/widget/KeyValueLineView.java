package com.penghaonan.appmanager.detail.card.widget;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.penghaonan.appmanager.R;

public class KeyValueLineView extends LinearLayout {
    public KeyValueLineView(Context context) {
        super(context);
        inflate(context, R.layout.view_key_value_line, this);
    }

    public void setKeyString(int id){
        ((TextView)findViewById(R.id.tv_key)).setText(id);
    }

    public void setKeyString(String str){
        ((TextView)findViewById(R.id.tv_key)).setText(str);
    }

    public void setValueString(String str){
        ((TextView)findViewById(R.id.tv_value)).setText(str);
    }
}
