package com.penghaonan.appmanager.detail;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseFragment;
import com.penghaonan.appmanager.detail.card.ACard;
import com.penghaonan.appmanager.detail.card.AppSettingCard;
import com.penghaonan.appmanager.detail.card.BackupCard;
import com.penghaonan.appmanager.detail.card.HeaderCard;
import com.penghaonan.appmanager.detail.card.PermissionCard;
import com.penghaonan.appmanager.detail.card.SummaryCard;
import com.penghaonan.appmanager.detail.card.appsize.AppSizeCard;
import com.penghaonan.appmanager.detail.card.flag.FlagCard;
import com.penghaonan.appmanager.manager.AppInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by likai on 15/10/28.
 * 应用详情页
 */
public class DetailFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private static final String TAG = "DetailFragment";
    public static final String EXTRAS_APPINFO = "app_info";
    private AppInfo mAppInfo;
    private ListView mCardListView;
    private CardListAdapter mAdapter;
    private List<ACard> mCardList = new ArrayList<>();

    public static DetailFragment newInstance(AppInfo info) {
        DetailFragment fragment = new DetailFragment();
        fragment.mAppInfo = info;
        return fragment;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail;
    }

    @Override
    public void initViews(View root) {
        mCardListView = (ListView) findViewById(R.id.lv_card_list);
    }

    @Override
    public void initDatas() {
        initCards();
        mAdapter = new CardListAdapter();
        mCardListView.setAdapter(mAdapter);
        mCardListView.setOnItemClickListener(this);
    }

    @Override
    public String getFragmentName() {
        return TAG;
    }

    private void initCards(){
        //头部卡片
        ACard card = new HeaderCard(getActivity(), mAppInfo);
        mCardList.add(card);

        //概述
        card = new SummaryCard(getActivity(), mAppInfo);
        mCardList.add(card);

        // 各种Flag信息
        card = new FlagCard(getActivity(), mAppInfo);
        mCardList.add(card);

        //空间占用
        card = new AppSizeCard(getActivity(), mAppInfo);
        mCardList.add(card);

        //备份
        card = new BackupCard(getActivity(), mAppInfo);
        mCardList.add(card);

        //权限
        card = new PermissionCard(getActivity(), mAppInfo);
        mCardList.add(card);

        //应用详情跳转
        card = new AppSettingCard(getActivity(), mAppInfo);
        mCardList.add(card);
    }

    private String formatTime(long time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.SIMPLIFIED_CHINESE);
        return sdf.format(time);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mAdapter.getItem(position).onCardClick();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (ACard card : mCardList){
            card.onDestroy();
        }
    }

    private class CardListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mCardList.size();
        }

        @Override
        public ACard getItem(int position) {
            return mCardList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getItem(position);
        }
    }
}
