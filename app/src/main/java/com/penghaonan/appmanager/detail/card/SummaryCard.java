package com.penghaonan.appmanager.detail.card;

import androidx.fragment.app.FragmentActivity;
import android.view.ViewGroup;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.detail.card.widget.KeyValueLineView;
import com.penghaonan.appmanager.manager.AppInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by likai on 15/11/16.
 */
public class SummaryCard extends ARadiusCard{

    public SummaryCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);

        ViewGroup contentView = (ViewGroup)findViewById(R.id.view_content);

        //version name
        KeyValueLineView lineView = new KeyValueLineView(activity);
        lineView.setKeyString(R.string.version_name);
        lineView.setValueString(mAppInfo.pkgInfo.versionName);
        contentView.addView(lineView);

        //version code
        lineView = new KeyValueLineView(activity);
        lineView.setKeyString(R.string.version_code);
        lineView.setValueString(mAppInfo.pkgInfo.versionCode+"");
        contentView.addView(lineView);

        //package name
        lineView = new KeyValueLineView(activity);
        lineView.setKeyString(R.string.package_name);
        lineView.setValueString(mAppInfo.getPackageName());
        contentView.addView(lineView);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //first install time
        lineView = new KeyValueLineView(activity);
        lineView.setKeyString(R.string.first_install_time);
        lineView.setValueString(format.format(new Date(mAppInfo.getInstallTime())));
        contentView.addView(lineView);

        //first install time
        lineView = new KeyValueLineView(activity);
        lineView.setKeyString(R.string.last_update_time);
        lineView.setValueString(format.format(new Date(mAppInfo.getUpdateTime())));
        contentView.addView(lineView);

        setTitle(R.string.summary);
    }

    @Override
    protected int layout() {
        return R.layout.card_summary;
    }

    @Override
    public void onCardClick() {

    }

}
