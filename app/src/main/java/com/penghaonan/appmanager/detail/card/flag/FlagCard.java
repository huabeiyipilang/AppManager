package com.penghaonan.appmanager.detail.card.flag;

import android.content.pm.ApplicationInfo;
import android.os.Build;
import androidx.fragment.app.FragmentActivity;
import android.view.ViewGroup;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.detail.card.ARadiusCard;
import com.penghaonan.appmanager.manager.AppInfo;


public class FlagCard extends ARadiusCard {
    public FlagCard(FragmentActivity activity, AppInfo appInfo) {
        super(activity, appInfo);

        ViewGroup container = findViewById(R.id.flag_container);
        container.addView(new FlagItemView(activity, R.string.app_flag_system_app, appInfo, ApplicationInfo.FLAG_SYSTEM));
        container.addView(new FlagItemView(activity, R.string.app_flag_debuggable, appInfo, ApplicationInfo.FLAG_DEBUGGABLE));
        container.addView(new FlagItemView(activity, R.string.app_flag_has_code, appInfo, ApplicationInfo.FLAG_HAS_CODE));
        container.addView(new FlagItemView(activity, R.string.app_flag_persistent, appInfo, ApplicationInfo.FLAG_PERSISTENT));
        container.addView(new FlagItemView(activity, R.string.app_flag_allow_clear_data, appInfo, ApplicationInfo.FLAG_ALLOW_CLEAR_USER_DATA));
        container.addView(new FlagItemView(activity, R.string.app_flag_updated_system_app, appInfo, ApplicationInfo.FLAG_UPDATED_SYSTEM_APP));
        container.addView(new FlagItemView(activity, R.string.app_flag_allow_backup, appInfo, ApplicationInfo.FLAG_ALLOW_BACKUP));
        container.addView(new FlagItemView(activity, R.string.app_flag_external_storage, appInfo, ApplicationInfo.FLAG_EXTERNAL_STORAGE));
        container.addView(new FlagItemView(activity, R.string.app_flag_stopped, appInfo, ApplicationInfo.FLAG_STOPPED));
        if (Build.VERSION.SDK_INT >= 21) {
            container.addView(new FlagItemView(activity, R.string.app_flag_game, appInfo, ApplicationInfo.FLAG_IS_GAME));
        }

    }

    @Override
    protected int layout() {
        return R.layout.card_flag;
    }

    @Override
    public void onCardClick() {

    }
}
