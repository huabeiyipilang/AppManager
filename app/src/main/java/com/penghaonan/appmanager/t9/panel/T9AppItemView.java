package com.penghaonan.appmanager.t9.panel;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseItemView;
import com.penghaonan.appmanager.t9.AppEntranceInfo;

public class T9AppItemView extends BaseItemView<AppEntranceInfo> {

    private ImageView iconView;
    private TextView titleView;

    public T9AppItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.item_view_t9_list;
    }

    @Override
    protected void initViews() {
        iconView = findViewById(R.id.app_icon);
        titleView = findViewById(R.id.app_title);
    }

    @Override
    public boolean bindData(AppEntranceInfo data) {
        titleView.setText(data.getEntName());
        if (data.iconLoaded()) {
            iconView.setImageDrawable(data.getEntryIcon());
            return false;
        }
        return true;
    }

    @Override
    protected void asyncBindData(AppEntranceInfo data) {
        super.asyncBindData(data);
        data.getEntryIcon();
        post(() -> iconView.setImageDrawable(data.getEntryIcon()));
    }
}
