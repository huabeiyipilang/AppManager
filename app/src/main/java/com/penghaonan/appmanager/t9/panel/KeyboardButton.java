package com.penghaonan.appmanager.t9.panel;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.penghaonan.appmanager.R;

import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class KeyboardButton extends LinearLayout {

    private TextView mLine1View, mLine2View;

    public KeyboardButton(Context context) {
        this(context, null);
    }

    public KeyboardButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.VERTICAL);
        inflate(context, R.layout.keyboard_button, this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(getDrawable(getContext(), android.R.attr.selectableItemBackground));
        }
        mLine1View = findViewById(R.id.tv_line1);
        mLine2View = findViewById(R.id.tv_line2);

        if (attrs != null) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.KeyboardButton);
            CharSequence line1 = ta.getText(R.styleable.KeyboardButton_textLineOne);
            CharSequence line2 = ta.getText(R.styleable.KeyboardButton_textLineTwo);
            mLine1View.setText(line1);
            mLine2View.setText(line2);
            ta.recycle();
        }
    }

    private Drawable getDrawable(@NonNull Context context, @AttrRes int attr) {
        TypedArray ta = context.obtainStyledAttributes(new int[]{
                attr
        });
        Drawable drawable = ta.getDrawable(0);
        ta.recycle();
        return drawable;
    }
}
