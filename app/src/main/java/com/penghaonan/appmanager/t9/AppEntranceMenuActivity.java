package com.penghaonan.appmanager.t9;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.penghaonan.appmanager.BlankActivity;
import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseActivity;
import com.penghaonan.appmanager.base.BaseTransparentActivity;
import com.penghaonan.appmanager.detail.DetailFragment;
import com.penghaonan.appmanager.utils.AppUtils;

import androidx.annotation.Nullable;

public class AppEntranceMenuActivity extends BaseTransparentActivity {
    public static final String EXTRA_ID = "app_entrance_id";
    private AppEntranceInfo appEntranceInfo;

    public static void showMenu(Context context, long id) {
        Intent intent = new Intent(context, AppEntranceMenuActivity.class);
        intent.putExtra(EXTRA_ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_entrance_menu);
        long id = getIntent().getLongExtra(EXTRA_ID, -1);
        if (id == -1) {
            finish();
            return;
        }
        appEntranceInfo = AppEntranceManager.getEntranceInfoById(id);
        if (appEntranceInfo == null) {
            finish();
            return;
        }

        ImageView iconView = findViewById(R.id.iv_icon);
        iconView.setImageDrawable(appEntranceInfo.getEntryIcon());

        TextView titleView = findViewById(R.id.tv_title);
        titleView.setText(appEntranceInfo.getEntName());

        findViewById(R.id.item_detail).setOnClickListener(v -> {
            DetailFragment detailFragment = DetailFragment.newInstance(appEntranceInfo.getAppInfo());
            BlankActivity.startFragment(detailFragment, true);
            finish();
        });

        findViewById(R.id.item_uninstall).setOnClickListener(v -> {
            AppUtils.uninstall(AppEntranceMenuActivity.this, appEntranceInfo.getPkgName());
            finish();
        });
    }
}
