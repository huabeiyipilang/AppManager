package com.penghaonan.appmanager.t9;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appmanager.manager.AppManager;
import com.penghaonan.appmanager.utils.ObjectBox;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import io.objectbox.Box;

public class AppEntranceManager {
    private static List<AppEntranceInfo> entranceInfoList;
    private static List<ResolveInfo> resolveInfoList;

    private static final String[] BLACK_LIST = {AppDelegate.getApp().getPackageName()};

    static {
        Arrays.sort(BLACK_LIST);
    }

    public static List<AppEntranceInfo> getEntranceInfoList() {
        if (entranceInfoList == null) {
            return new LinkedList<>();
        }
        List<AppEntranceInfo> res = new LinkedList<>();
        for (AppEntranceInfo info : entranceInfoList) {
            if (Arrays.binarySearch(BLACK_LIST, info.getPkgName()) < 0) {
                res.add(info);
            }
        }
        return res;
    }

    public static AppEntranceInfo getEntranceInfoById(long id) {
        for (AppEntranceInfo info : entranceInfoList) {
            if (info.id == id) {
                return info;
            }
        }
        return null;
    }

    public static void startLoad() {
        entranceInfoList = new LinkedList<>();
        resolveInfoList = new LinkedList<>();
        PackageManager packageManager = AppDelegate.getApp().getPackageManager();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = packageManager.queryIntentActivities(intent, PackageManager.GET_RESOLVED_FILTER);
        if (resolveInfos != null) {
            resolveInfoList.addAll(resolveInfos);
        }

        Box<AppEntranceInfo> box = ObjectBox.get().boxFor(AppEntranceInfo.class);
        List<AppEntranceInfo> appEntranceInfos = box.getAll();
        if (appEntranceInfos != null) {
            entranceInfoList.addAll(appEntranceInfos);
        }

        List<ResolveInfo> resolveInfoCheckList = new LinkedList<>(resolveInfoList);

        // 数据库中的信息
        Iterator<AppEntranceInfo> iterator = entranceInfoList.iterator();
        while (iterator.hasNext()) {
            AppEntranceInfo appEntranceInfo = iterator.next();
            boolean found = false;
            for (ResolveInfo resolveInfo : resolveInfoCheckList) {
                if (resolveInfo.activityInfo != null
                        && TextUtils.equals(appEntranceInfo.getPkgName(), resolveInfo.activityInfo.packageName)
                        && TextUtils.equals(appEntranceInfo.getClsName(), resolveInfo.activityInfo.name)) {
                    appEntranceInfo.setResolveInfo(resolveInfo);
                    appEntranceInfo.setAppInfo(AppManager.getInstance().findAppInfoByPkgName(resolveInfo.activityInfo.packageName));
                    resolveInfoCheckList.remove(resolveInfo);
                    found = true;
                    break;
                }
            }
            // 数据库中的组件找到，则删除
            if (!found) {
                box.remove(appEntranceInfo);
                iterator.remove();
            }
        }

        //扫描到的组件，数据库中没有
        for (ResolveInfo resolveInfo : resolveInfoCheckList) {
            AppEntranceInfo appEntranceInfo = new AppEntranceInfo();
            appEntranceInfo.setResolveInfo(resolveInfo);
            appEntranceInfo.setAppInfo(AppManager.getInstance().findAppInfoByPkgName(resolveInfo.activityInfo.packageName));
            appEntranceInfo.load();
            box.put(appEntranceInfo);
            entranceInfoList.add(appEntranceInfo);
        }
    }
}
