package com.penghaonan.appmanager.t9;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appmanager.manager.AppInfo;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.NonNull;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.annotation.Index;
import io.objectbox.annotation.Transient;

@Entity
public class AppEntranceInfo implements Serializable {
    @Id
    long id;
    @Transient
    private ResolveInfo resolveInfo;
    @Transient
    private AppInfo appInfo;

    @Index
    private String pkgName;

    private String clsName;

    @Index
    private String entName;
    @Transient
    private Drawable entryIcon;
    @Index
    private String entNamePinyin;
    @Index
    private String entNameT9;

    private long openCount;

    public void setResolveInfo(ResolveInfo resolveInfo) {
        this.resolveInfo = resolveInfo;
        pkgName = this.resolveInfo.activityInfo.packageName;
        clsName = this.resolveInfo.activityInfo.name;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    void load() {
        getEntName();
        getEntNamePinyin();
        getEntNameT9();
    }

    public long getId() {
        return id;
    }

    public String getPkgName() {
        return pkgName;
    }

    public String getClsName() {
        return clsName;
    }

    public ComponentName getComponentName() {
        return new ComponentName(pkgName, clsName);
    }

    public String getEntName() {
        if (TextUtils.isEmpty(entName)) {
            entName = (String) AppDelegate.getApp().getPackageManager().getApplicationLabel(appInfo.pkgInfo.applicationInfo);
        }
        return String.valueOf(entName);
    }

    public Drawable getEntryIcon() {
        if (entryIcon == null) {
            if (resolveInfo != null && resolveInfo.activityInfo != null) {
                ComponentName componentName = new ComponentName(appInfo.getPackageName(), resolveInfo.activityInfo.name);
                try {
                    entryIcon = AppDelegate.getApp().getPackageManager().getActivityIcon(componentName);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return entryIcon;
    }

    public boolean iconLoaded() {
        return entryIcon != null;
    }

    public String getEntNamePinyin() {
        if (TextUtils.isEmpty(entNamePinyin)) {
            List<String> pinyins = T9Utils.str2Pinyin(getEntName());
            StringBuilder sb = new StringBuilder();
            for (String pinyin : pinyins) {
                sb.append(" ").append(pinyin);
            }
            entNamePinyin = sb.toString();
        }
        return entNamePinyin;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public ResolveInfo getResolveInfo() {
        return resolveInfo;
    }

    public String getEntNameT9() {
        if (TextUtils.isEmpty(entNameT9)) {
            entNameT9 = T9Utils.str2T9(getEntNamePinyin());
        }
        return entNameT9;
    }

    public long getOpenCount() {
        return openCount;
    }

    public void setOpenCount(long openCount) {
        this.openCount = openCount;
    }

    @NonNull
    @Override
    public String toString() {
        return entName + " open count:" + getOpenCount();
    }
}
