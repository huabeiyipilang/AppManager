package com.penghaonan.appmanager.t9.panel;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.penghaonan.appmanager.R;

import java.util.Arrays;

public class T9KeyboardView extends FrameLayout {

    public static final String[] KEY_DIGITAL = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    public static final String KEY_OPEN = "open";

    static {
        Arrays.sort(KEY_DIGITAL);
    }

    public interface OnKeyClickListener{
        void onKeyClick(String key);
        void onKeyLongClick(String key);
    }

    private OnKeyClickListener onKeyClickListener;

    public T9KeyboardView(@NonNull Context context) {
        this(context, null);
    }

    public T9KeyboardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public void setOnKeyClickListener(OnKeyClickListener onKeyClickListener) {
        this.onKeyClickListener = onKeyClickListener;
    }

    public static boolean isDigitalKey(String key) {
        if (TextUtils.isEmpty(key)) {
            return false;
        }
        return Arrays.binarySearch(KEY_DIGITAL, key) >= 0;
    }

    private void initView() {
        inflate(getContext(), R.layout.keyboard_t9, this);

        findViewById(R.id.bt_0).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_1).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_2).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_3).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_4).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_5).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_6).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_7).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_8).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_9).setOnClickListener(onDigitalBtnClickListener);
        findViewById(R.id.bt_open).setOnClickListener(onDigitalBtnClickListener);

        findViewById(R.id.bt_0).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_1).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_2).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_3).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_4).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_5).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_6).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_7).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_8).setOnLongClickListener(onDigitalBtnLongClickListener);
        findViewById(R.id.bt_9).setOnLongClickListener(onDigitalBtnLongClickListener);

        setClickable(true);
    }

    private View.OnClickListener onDigitalBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = (String) v.getTag();
            if (onKeyClickListener != null) {
                onKeyClickListener.onKeyClick(tag);
            }
        }
    };

    private View.OnLongClickListener onDigitalBtnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            String tag = (String) v.getTag();
            if (onKeyClickListener != null) {
                onKeyClickListener.onKeyLongClick(tag);
                return true;
            }
            return false;
        }
    };
}
