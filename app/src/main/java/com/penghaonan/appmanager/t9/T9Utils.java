package com.penghaonan.appmanager.t9;

import android.text.TextUtils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class T9Utils {
    public static final String CHAR_SEPARATOR = "";
    public static final Map<Character, Character> T9Map = new HashMap<>();

    static {
        T9Map.put('a', '2');
        T9Map.put('b', '2');
        T9Map.put('c', '2');
        T9Map.put('d', '3');
        T9Map.put('e', '3');
        T9Map.put('f', '3');
        T9Map.put('g', '4');
        T9Map.put('h', '4');
        T9Map.put('i', '4');
        T9Map.put('j', '5');
        T9Map.put('k', '5');
        T9Map.put('l', '5');
        T9Map.put('m', '6');
        T9Map.put('n', '6');
        T9Map.put('o', '6');
        T9Map.put('p', '7');
        T9Map.put('q', '7');
        T9Map.put('r', '7');
        T9Map.put('s', '7');
        T9Map.put('t', '8');
        T9Map.put('u', '8');
        T9Map.put('v', '8');
        T9Map.put('w', '9');
        T9Map.put('x', '9');
        T9Map.put('y', '9');
        T9Map.put('z', '9');
        T9Map.put('0', '0');
        T9Map.put('1', '1');
        T9Map.put('2', '2');
        T9Map.put('3', '3');
        T9Map.put('4', '4');
        T9Map.put('5', '5');
        T9Map.put('6', '6');
        T9Map.put('7', '7');
        T9Map.put('8', '8');
        T9Map.put('9', '9');
        T9Map.put(' ', ' ');
    }

    public static List<String> str2Pinyin(String input) {
        if (TextUtils.isEmpty(input)) {
            return Collections.emptyList();
        }
        HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
        outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
//        outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
        outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);

        char[] inputChars = input.toCharArray();
        List<Set<String>> toneTree = new LinkedList<>();
        for (char c : inputChars) {
            if (isChineseChar(c)) {
                try {
                    String[] tones = PinyinHelper.toHanyuPinyinStringArray(c, outputFormat);
                    Set<String> toneSet = new HashSet<>(Arrays.asList(tones));
                    toneTree.add(toneSet);
                } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
                    badHanyuPinyinOutputFormatCombination.printStackTrace();
                }
            } else {
                Set<String> set = new HashSet<>();
                set.add(String.valueOf(c).toLowerCase());
                toneTree.add(set);
            }
        }
        List<String> outputs = new LinkedList<>();
        // 取出每一个字符的所有发音
        for (Set<String> tones : toneTree) {
            if (tones == null) {
                continue;
            }
            // 取出每一个发音，追加到之前列表结尾
            List<String> oldOutputs = outputs;
            outputs = new LinkedList<>();
            for (String tone : tones) {
                if (oldOutputs.size() == 0) {
                    outputs.add(tone + CHAR_SEPARATOR);
                } else {
                    for (String oldString : oldOutputs) {
                        outputs.add(oldString + tone + CHAR_SEPARATOR);
                    }
                }
            }
        }
        return outputs;
    }

    public static String str2T9(String input) {
        if (TextUtils.isEmpty(input)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (char c : input.toCharArray()) {
            Character cOutput = T9Map.get(c);
            if (cOutput == null) {
                cOutput = '1';
            }
            sb.append(cOutput);
        }
        return sb.toString();
    }

    public static boolean isChineseChar(char c) {
        return String.valueOf(c).matches("[\u4e00-\u9fa5]");
    }

}
