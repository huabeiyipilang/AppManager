package com.penghaonan.appmanager.t9;

import android.os.Bundle;

import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.BaseTransparentActivity;
import com.penghaonan.appmanager.t9.panel.MainPanel;

import androidx.annotation.Nullable;

public class T9PanelActivity extends BaseTransparentActivity {

    private MainPanel mainPanel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t9_panel);
        findViewById(R.id.view_activity_root).setOnClickListener(v -> finish());
        mainPanel = findViewById(R.id.view_panel);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPanel.onPanelShow();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainPanel.onPanelHide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPanel.release();
    }
}
