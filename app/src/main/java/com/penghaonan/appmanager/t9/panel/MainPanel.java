package com.penghaonan.appmanager.t9.panel;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.utils.CollectionUtils;
import com.penghaonan.appframework.utils.Logger;
import com.penghaonan.appframework.utils.ToastUtils;
import com.penghaonan.appmanager.BlankActivity;
import com.penghaonan.appmanager.DataLoader;
import com.penghaonan.appmanager.R;
import com.penghaonan.appmanager.base.ItemAdapter;
import com.penghaonan.appmanager.detail.DetailFragment;
import com.penghaonan.appmanager.t9.AppEntranceInfo;
import com.penghaonan.appmanager.t9.AppEntranceManager;
import com.penghaonan.appmanager.t9.AppEntranceMenuActivity;
import com.penghaonan.appmanager.utils.ObjectBox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class MainPanel extends FrameLayout {

    private T9KeyboardView keyboardView;
    private EditText inputEditText;
    private AbsListView mListView;
    private View inputClearView;

    private ItemAdapter<AppEntranceInfo> mItemAdapter;
    private SearchTaskQueue mTaskQueue = new SearchTaskQueue();

    private List<AppEntranceInfo> mAppList = new ArrayList<>();
    private boolean isShow = true;

    private T9KeyboardView.OnKeyClickListener onKeyClickListener = new T9KeyboardView.OnKeyClickListener() {
        @Override
        public void onKeyClick(String key) {
            if (T9KeyboardView.isDigitalKey(key)) {
                inputEditText.append(key);
            } else if (T9KeyboardView.KEY_OPEN.equals(key)) {
                if (mItemAdapter.getCount() > 0) {
                    AppEntranceInfo appEntranceInfo = mItemAdapter.getItem(0);
                    openApp(appEntranceInfo);
                }
            }
        }

        @Override
        public void onKeyLongClick(String key) {

        }
    };

    private BroadcastReceiver mLocalReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent == null ? "" : intent.getAction();
            if (DataLoader.ACTION_DATA_PREPARED.equals(action)) {
                updateUI();
            }
        }
    };

    public MainPanel(@NonNull Context context) {
        this(context, null);
    }

    public MainPanel(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    private void initViews() {
        inflate(getContext(), R.layout.main_panel, this);
        mListView = findViewById(R.id.app_list_view);
        inputEditText = findViewById(R.id.et_input);
        keyboardView = findViewById(R.id.view_keyboard);
        inputClearView = findViewById(R.id.iv_clear);

        inputClearView.setVisibility(View.GONE);

        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Logger.i("afterTextChanged : " + s);
                if (s == null || s.length() == 0) {
                    mTaskQueue.addSearchKey("");
                    inputClearView.setVisibility(View.GONE);
                } else {
                    mTaskQueue.addSearchKey(s.toString());
                    inputClearView.setVisibility(View.VISIBLE);
                }
            }
        });
        inputClearView.setOnClickListener(v -> inputEditText.getText().clear());
        keyboardView.setOnKeyClickListener(onKeyClickListener);

        mItemAdapter = new ItemAdapter<>(mAppList, T9AppItemView.class);
        mListView.setAdapter(mItemAdapter);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            AppEntranceInfo appEntranceInfo = mItemAdapter.getItem(position);
            openApp(appEntranceInfo);
        });
        mListView.setOnItemLongClickListener((parent, view, position, id) -> {
            AppEntranceInfo appEntranceInfo = mItemAdapter.getItem(position);
            AppEntranceMenuActivity.showMenu(getContext(), appEntranceInfo.getId());
            return true;
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction(DataLoader.ACTION_DATA_PREPARED);
        LocalBroadcastManager.getInstance(AppDelegate.getApp()).registerReceiver(mLocalReceiver, filter);

        updateUI();
    }

    private void openApp(AppEntranceInfo appEntranceInfo) {
        ActivityInfo activityInfo = appEntranceInfo.getResolveInfo().activityInfo;
        ComponentName componentName = new ComponentName(activityInfo.packageName, activityInfo.name);
        Intent intent = new Intent();
        intent.setComponent(componentName);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (getContext() instanceof Activity) {
            ((Activity) getContext()).finish();
        }
        List<ResolveInfo> resolveInfoList = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.GET_RESOLVED_FILTER);
        if (CollectionUtils.isEmpty(resolveInfoList)) {
            ToastUtils.showToast(getContext().getString(R.string.open_app_failed_reload));
            DataLoader.startLoad();
        } else {
            appEntranceInfo.setOpenCount(appEntranceInfo.getOpenCount() + 1);
            long key = ObjectBox.get().boxFor(AppEntranceInfo.class).put(appEntranceInfo);
            Logger.i("key:" + key + ", " + appEntranceInfo.toString());
            AppDelegate.getApp().startActivity(intent);
        }
    }

    private void updateUI() {
        if (!isShow) {
            return;
        }
        Editable s = inputEditText.getText();
        if (s == null || s.length() == 0) {
            mTaskQueue.addSearchKey("");
        } else {
            mTaskQueue.addSearchKey(s.toString());
        }
    }

    public void release() {
        LocalBroadcastManager.getInstance(AppDelegate.getApp()).unregisterReceiver(mLocalReceiver);
        mItemAdapter.release();
        mTaskQueue.release();
    }

    public void onPanelShow() {
        isShow = true;
        updateUI();
    }

    public void onPanelHide() {
        isShow = false;
    }

    private Comparator<AppEntranceInfo> appEntranceInfoComparator = (o1, o2) -> {
        long compareValue = o2.getOpenCount() - o1.getOpenCount();
        int res;
        if (compareValue == 0) {
            res = o1.getEntName().compareTo(o2.getEntName());
        } else {
            res = compareValue > 0 ? 1 : -1;
        }
        return res;
    };

    private class SearchTaskQueue {
        private String waitingKey;
        private boolean running;
        private ExecutorService executor = Executors.newSingleThreadExecutor();

        SearchTask searchTask = new SearchTask() {
            @Override
            protected void onTaskFinished() {
                super.onTaskFinished();
                if (waitingKey != null) {
                    submitSearchTask(waitingKey);
                    waitingKey = null;
                } else {
                    running = false;
                }
            }
        };

        void addSearchKey(String key) {
            if (running) {
                waitingKey = key;
            } else {
                submitSearchTask(key);
            }
        }

        private void submitSearchTask(String key) {
            running = true;
            searchTask.setKey(key);
            executor.submit(searchTask);
        }

        void release() {
            executor.shutdown();
        }

    }

    private class SearchTask implements Runnable {

        private String key;

        SearchTask() {
        }

        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public void run() {
            Logger.i("SearchTask start. key:" + key);
            List<AppEntranceInfo> entrances = AppEntranceManager.getEntranceInfoList();
            List<AppEntranceInfo> result;
            if (TextUtils.isEmpty(key)) {
                result = entrances;
            } else {
                result = new LinkedList<>();
                for (AppEntranceInfo info : entrances) {
                    String t9 = info.getEntNameT9();
                    if (!TextUtils.isEmpty(t9) && t9.contains(key)) {
                        result.add(info);
                    }
                }
            }

            // 排序
            Collections.sort(result, appEntranceInfoComparator);

            AppDelegate.post(() -> {
                if (!isShow) {
                    return;
                }
                Logger.i("update resule size:" + result.size());
                mAppList.clear();
                mAppList.addAll(result);
                mItemAdapter.setData(mAppList);
                mItemAdapter.notifyDataSetChanged();
                Logger.i("notifyDataSetChanged size:" + mItemAdapter.getCount());
                onTaskFinished();
            });
        }

        protected void onTaskFinished() {
            Logger.i("SearchTask finish. key:" + key);
        }
    }
}
