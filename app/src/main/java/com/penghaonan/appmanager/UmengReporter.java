package com.penghaonan.appmanager;

import android.app.Activity;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.base.BaseFrameworkFragment;
import com.penghaonan.appframework.reporter.IReporter;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

public class UmengReporter implements IReporter {

    public UmengReporter() {
        MobclickAgent.setDebugMode(BuildConfig.DEBUG);
        MobclickAgent.setScenarioType(AppDelegate.getApp(), MobclickAgent.EScenarioType.E_UM_NORMAL);
    }

    @Override
    public void setChannel(String channel) {
    }

    @Override
    public void onActivityResume(@NonNull Activity activity) {
        MobclickAgent.onPageStart(activity.getClass().getSimpleName());
        MobclickAgent.onResume(activity);
    }

    @Override
    public void onActivityPause(@NonNull Activity activity) {
        MobclickAgent.onPageEnd(activity.getClass().getSimpleName());
        MobclickAgent.onPause(activity);
    }

    @Override
    public void onFragmentResume(@NonNull BaseFrameworkFragment fragment) {
        MobclickAgent.onPageStart(fragment.getFragmentName());
    }

    @Override
    public void onFragmentPause(@NonNull BaseFrameworkFragment fragment) {
        MobclickAgent.onPageEnd(fragment.getFragmentName());
    }

    @Override
    public void onEvent(String eventId) {
        MobclickAgent.onEvent(AppDelegate.getApp(), eventId);
    }

    @Override
    public void onEvent(String eventId, String value) {
        HashMap<String, String> map = new HashMap<>();
        map.put("value", value);
        MobclickAgent.onEvent(AppDelegate.getApp(), eventId, map);
    }

    @Override
    public void onEvent(String eventId, Map<String, String> values) {
        MobclickAgent.onEvent(AppDelegate.getApp(), eventId, values);
    }
}
