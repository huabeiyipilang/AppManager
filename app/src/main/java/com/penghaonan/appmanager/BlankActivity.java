package com.penghaonan.appmanager;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;

import com.penghaonan.appmanager.base.BaseActivity;
import com.penghaonan.appmanager.base.BaseFragment;
import com.penghaonan.appmanager.utils.DataTransfer;

public class BlankActivity extends BaseActivity {

    public static final String INTENT_FRAGMENT_NAME = "intent_fragment_name";

    public static void startFragment(BaseFragment fragment, boolean newTask) {
        int index = DataTransfer.putData(fragment);
        Intent intent = new Intent(App.getApp(), BlankActivity.class);
        intent.putExtra(INTENT_FRAGMENT_NAME, index);
        if (newTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        App.getApp().startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
        int index = getIntent().getIntExtra(INTENT_FRAGMENT_NAME, 0);
        BaseFragment fragment = (BaseFragment) DataTransfer.getData(index);
        if (fragment != null) {
            setContentFragment(fragment);
        }
    }

    private void setContentFragment(BaseFragment fragment) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.content_view, fragment);
        t.commit();
    }
}
