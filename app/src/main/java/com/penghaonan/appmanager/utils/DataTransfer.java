package com.penghaonan.appmanager.utils;

import android.util.SparseArray;

/**
 * Created by likai on 15/10/30.
 * 数据传递
 */
public class DataTransfer {

    private static final long DEFAULT_PERSIST_TIME = 10 * 1000;

    private static int sIndex = 0;

    private static SparseArray<Data> sDataContainer = new SparseArray<>();

    public static int putData(Object data){
        Data d = new Data();
        d.content = data;
        d.deadTime = System.currentTimeMillis() + DEFAULT_PERSIST_TIME;
        int index = sIndex++;
        sDataContainer.put(index, d);
        return index;
    }

    public static Object getData(int index){
        Data data = sDataContainer.get(index);
        sDataContainer.remove(index);
        if (data == null){
            return null;
        }
        return data.content;
    }

    public void clearOverTimeData(){
        for (int i = 0; i < sDataContainer.size(); i++){
            Data d = sDataContainer.valueAt(i);
            if (d.deadTime > System.currentTimeMillis()){

            }
        }
    }

    private static class Data{
        Object content;
        long deadTime;
    }

}
