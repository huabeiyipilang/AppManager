package com.penghaonan.appmanager.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

/**
 * Created by baidu on 15/11/18.
 */
public class BatteryUtils {
    public static int getBatteryLevel(Context context){

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.registerReceiver(null, filter);
        if (intent == null){
            return -1;
        }
        return intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
    }
}
