package com.penghaonan.appmanager.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.penghaonan.appframework.utils.Logger;

public class AppUtils {
    /**
     * 卸载
     */
    public static void uninstall(Context context, String pkg) {
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + pkg));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
