package com.penghaonan.appmanager.utils;

import android.content.Context;

import com.penghaonan.appmanager.BuildConfig;
import com.penghaonan.appmanager.t9.MyObjectBox;

import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

public class ObjectBox {
    private static BoxStore boxStore;

    public static void init(Context context) {
        boxStore = MyObjectBox.builder()
                .androidContext(context.getApplicationContext())
                .build();
        if (BuildConfig.DEBUG) {//开启浏览器访问ObjectBox
            boolean started = new AndroidObjectBrowser(boxStore).start(context);
        }
    }

    public static BoxStore get() { return boxStore; }
}
