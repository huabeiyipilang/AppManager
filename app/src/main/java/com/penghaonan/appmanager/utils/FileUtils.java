package com.penghaonan.appmanager.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by carl on 16/3/25.
 */
public class FileUtils {
    public static boolean copyFile(File oldFile, File newFile) {
        try {
            int bytesum = 0;
            int byteread = 0;
            if (!oldFile.exists()) { //文件不存在时
                InputStream inStream = new FileInputStream(oldFile.getAbsolutePath()); //读入原文件
                FileOutputStream fs = new FileOutputStream(newFile.getAbsolutePath());
                byte[] buffer = new byte[1024];
                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
