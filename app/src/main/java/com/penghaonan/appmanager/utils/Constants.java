package com.penghaonan.appmanager.utils;

/**
 * Created by carl on 12/27/15.
 */
public class Constants {
    public final static int CHART_COLOR_RED = 0xffff7438;
    public final static int CHART_COLOR_GREEN = 0xff81ff38;
    public final static int CHART_COLOR_BLUE = 0xff029ed9;
}
