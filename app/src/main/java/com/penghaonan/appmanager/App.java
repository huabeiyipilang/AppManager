package com.penghaonan.appmanager;

import android.os.Environment;
import androidx.multidex.MultiDexApplication;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.reporter.Reporter;
import com.penghaonan.appframework.utils.Logger;
import com.penghaonan.appframework.utils.ToastUtils;
import com.penghaonan.appmanager.utils.ObjectBox;

import java.io.File;

public class App extends MultiDexApplication {

    private static App sInstance;

    public static App getApp() {
        return sInstance;
    }

    public static File getExternalFolder() {
        File dir = Environment.getExternalStorageDirectory();
        dir = new File(dir, "AppManager");
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            ToastUtils.showToast(R.string.storage_not_available);
            return null;
        }
        if (!dir.exists()) {
            boolean result = dir.mkdirs();
            if (!result) {
                ToastUtils.showToast(R.string.storage_not_available);
                return null;
            }
        }
        return dir;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        Logger.setEnable(BuildConfig.DEBUG);
        AppDelegate.init(this);
        Reporter.getInstance().addReporter(new FirebaseReporter());
        Reporter.getInstance().addReporter(new UmengReporter());
        ObjectBox.init(this);

        AppReceiver.registerReceiver();

        DataLoader.startLoad();
    }
}
