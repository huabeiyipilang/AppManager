package com.penghaonan.appmanager.manager;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.content.pm.PermissionInfo;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.penghaonan.appmanager.t9.T9Utils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by likai on 15/10/22.
 * 应用信息
 */
public class AppInfo implements Serializable {

    public static final int FLAG_SYSTEM = 1 << 0;
    public static final int FLAG_UPDATED_SYSTEM_APP = 1 << 7;

    public PackageInfo pkgInfo;
    static PackageManager sPkgmanager;
    private WeakReference<Drawable> mIcon;
    private String mAppName;
    private String mAppNamePinyin;
    private String mAppNameT9;
    PackageStats mPkgStats;
    private List<PermissionInfo> mPermissions;

    public String getPackageName() {
        return pkgInfo.packageName;
    }

    /**
     * 获取图标
     */
    public Drawable getIcon() {
        if (mIcon == null || mIcon.get() == null) {
            mIcon = new WeakReference<>(sPkgmanager.getApplicationIcon(pkgInfo.applicationInfo));
        }
        return mIcon.get();
    }

    /**
     * 获取缓存图标
     */
    public Drawable getIconCache() {
        if (mIcon != null) {
            return mIcon.get();
        }
        return null;
    }

    public String getAppName() {
        if (TextUtils.isEmpty(mAppName)) {
            mAppName = sPkgmanager.getApplicationLabel(pkgInfo.applicationInfo).toString();
        }
        return mAppName;
    }

    public boolean isFlag(int flag) {
        return (pkgInfo.applicationInfo.flags & flag) != 0;
    }

    public boolean isSystemApp() {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    public boolean isSystemUpdateApp() {
        return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0);
    }

    public boolean isUserApp() {
        return (!isSystemApp() && !isSystemUpdateApp());
    }

    public long getInstallTime() {
        return pkgInfo.firstInstallTime;
    }

    public long getUpdateTime() {
        return pkgInfo.lastUpdateTime;
    }

    public PackageStats getPkgStats() {
        return mPkgStats;
    }

    public List<PermissionInfo> getPermissionInfos() {
        if (mPermissions == null) {
            mPermissions = new ArrayList<>();
            PermissionInfo perm;
            try {
                PackageInfo info = sPkgmanager.getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);
                if (info != null && info.requestedPermissions != null) {
                    pkgInfo.requestedPermissions = info.requestedPermissions;
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
            if (pkgInfo.requestedPermissions != null) {
                for (int i = 0; i < pkgInfo.requestedPermissions.length; i++) {
                    try {
                        perm = sPkgmanager.getPermissionInfo(pkgInfo.requestedPermissions[i], 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        continue;
                    }
                    mPermissions.add(perm);
                }
            }
        }
        return mPermissions;
    }

    public int getVersionCode() {
        return pkgInfo.versionCode;
    }

    public String getAppNamePinyin() {
        if (TextUtils.isEmpty(mAppNamePinyin)) {
            List<String> pinyins = T9Utils.str2Pinyin(getAppName());
            StringBuilder sb = new StringBuilder();
            for (String pinyin : pinyins) {
                sb.append(" ").append(pinyin);
            }
            mAppNamePinyin = sb.toString();
        }
        return mAppNamePinyin;
    }

    public String getAppNameT9() {
        if (TextUtils.isEmpty(mAppNameT9)) {
            mAppNameT9 = T9Utils.str2T9(getAppNamePinyin());
        }
        return mAppNameT9;
    }

    @Override
    public String toString() {
        return "pkg:" + pkgInfo.packageName;
    }
}
