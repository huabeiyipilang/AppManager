package com.penghaonan.appmanager.manager;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appmanager.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by likai on 16/3/28.
 */
public class BackupTask extends Thread {

    private AppInfo appInfo;
    private BackupManager.BackupListener listener;
    private boolean cancelled;
    private int resCode;

    BackupTask(AppInfo info, BackupManager.BackupListener listener) {
        appInfo = info;
        this.listener = listener;
    }

    @Override
    public void run() {
        super.run();
        backupApp();
        AppDelegate.post(new Runnable() {
            @Override
            public void run() {
                onFinish();
            }
        });
    }

    public void cancel() {
        cancelled = true;
    }

    private void callback(int code) {
        if (listener != null) {
            listener.onBackupCallback(code);
        }
    }

    private void onFinish() {
        BackupManager.sBackupThreadMap.remove(appInfo.getPackageName());
        callback(resCode);
    }


    private void backupApp() {
        //检查备份文件夹是否存在
        File dir = new File(App.getExternalFolder(), BackupManager.BACKUP_FOLDER);
        if (!dir.exists()) {
            boolean res = dir.mkdirs();
            if (!res) {
                resCode = BackupManager.BackupListener.RES_STORAGE_NOT_AVAILABLE;
                return;
            }
        }

        //查看是否有备份
        BackupManager.BackupInfo backupInfo = BackupManager.getBackupInfo(appInfo);
        if (backupInfo.state == BackupManager.BackupInfo.STATE_BACKUPED) {
            resCode = BackupManager.BackupListener.RES_BACKUPED;
            return;
        }

        //判断原始文件是否存在
        File sourceDir = new File(appInfo.pkgInfo.applicationInfo.publicSourceDir);
        if (!sourceDir.exists()) {
            resCode = BackupManager.BackupListener.RES_FAILED;
            return;
        }

        //生产文件名
        String fileName = appInfo.getAppName() + BackupManager.FILE_NAME_DIVIDER + appInfo.getPackageName() + BackupManager.FILE_NAME_DIVIDER + appInfo.getVersionCode() + BackupManager.FILE_NAME_SUFFIX;
        File dirFile = new File(dir, fileName);

        //拷贝文件
        boolean res = backupApplication(sourceDir, dirFile);
        if (cancelled) {
            resCode = BackupManager.BackupListener.RES_CANNCELLED;
            if (dirFile.exists()) {
                dirFile.delete();
            }
            return;
        } else {
            if (!res) {
                resCode = BackupManager.BackupListener.RES_FAILED;
                return;
            }

            //删除之前的备份
            backupInfo.delete();
        }
        resCode = BackupManager.BackupListener.RES_SUCCESS;
    }

    private boolean backupApplication(File sourceFile, File destFile) {
        // check file /data/app/appId-1.apk exists
        if (!sourceFile.exists()) {
            return false;
        }
        FileInputStream in = null;
        try {
            in = new FileInputStream(sourceFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        // create dest folder if necessary
        // do file copy operation
        byte[] c = new byte[1024];
        int slen;
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(destFile);
            while ((slen = in.read(c, 0, c.length)) != -1) {
                if (cancelled) {
                    return false;
                }
                out.write(c, 0, slen);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
