package com.penghaonan.appmanager.manager;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.penghaonan.appframework.AppDelegate;
import com.penghaonan.appframework.utils.Logger;
import com.penghaonan.appframework.utils.TimeCounter;
import com.penghaonan.appmanager.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Created by baidu on 15/10/22.
 */
public class AppManager {
    private static AppManager sInstance;
    private Context mContext;
    private PackageManager mPkgManager;
    private ScanAppListener mScanListener;
    private AtomicBoolean mIsScanning = new AtomicBoolean(false);
    private List<AppInfo> mAppList = new ArrayList<>();
    private Handler mMainHandler = new Handler(Looper.getMainLooper());

    private AppManager(Context context) {
        mContext = context;
        mPkgManager = mContext.getPackageManager();
        AppInfo.sPkgmanager = mPkgManager;
    }

    public interface ScanAppListener {
        void onProgress(int progress, int total);

        void onFinish();

        void onProgressShow(int msg);
    }

    public static AppManager getInstance() {
        return getInstance(AppDelegate.getApp());
    }

    public static AppManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppManager(context);
        }
        return sInstance;
    }

    public List<AppInfo> getAppList(boolean withSysApp) {
        if (withSysApp) {
            return new ArrayList<>(mAppList);
        } else {
            List<AppInfo> res = new ArrayList<>();
            for (AppInfo info : mAppList) {
                if (!info.isSystemApp()) {
                    res.add(info);
                }
            }
            return res;
        }
    }

    public AppInfo findAppInfoByPkgName(String pkg) {
        for (AppInfo appInfo : mAppList) {
            if (TextUtils.equals(appInfo.getPackageName(), pkg)) {
                return appInfo;
            }
        }
        return null;
    }

    public void scanApps(final ScanAppListener listener) {
        if (!mIsScanning.getAndSet(true)) {
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    List<PackageInfo> pkgList = mPkgManager.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);
                    mAppList.clear();

                    //扫描应用
                    mMainHandler.post(() -> listener.onProgressShow(R.string.launcher_progress_scan_apps));
                    final int size = pkgList.size();
                    for (int i = 0; i < size; i++) {
                        AppInfo ainfo = new AppInfo();
                        ainfo.pkgInfo = pkgList.get(i);
//                        ainfo.getPermissionInfos();//加载权限
                        mAppList.add(ainfo);
                        final int progress = i;
                        if (listener != null) {
                            mMainHandler.post(() -> listener.onProgress(progress, size));
                        }
                    }

                    //排序
//                    if (listener != null) {
//                        mMainHandler.post(() -> listener.onProgressShow(R.string.launcher_progress_sort_apps));
//                    }
//                    Collections.sort(mAppList, (lhs, rhs) -> lhs.getAppName().compareTo(rhs.getAppName()));

                    if (listener != null) {
                        mMainHandler.post(listener::onFinish);
                    }
                    mIsScanning.set(false);
                }
            }.start();
        }
    }

}
