package com.penghaonan.appmanager.manager;

import android.text.TextUtils;

import com.penghaonan.appmanager.App;
import com.penghaonan.appmanager.R;

import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.ConcurrentHashMap;

public class BackupManager {

    static final String BACKUP_FOLDER = "backup";
    static final String FILE_NAME_DIVIDER = "-";
    static final String FILE_NAME_SUFFIX = ".apk";

    static ConcurrentHashMap<String, BackupTask> sBackupThreadMap = new ConcurrentHashMap<>();

    public static class BackupInfo {
        public final static int STATE_NOT_BACKUP = 1;//未备份
        public final static int STATE_BACKUPED = 2;//已备份
        public final static int STATE_HAS_NEW_VERSION = 3;//需更新备份
        public final static int STATE_BACKUP_IS_NEW = 4;//备份版本较高

        public int state = STATE_NOT_BACKUP;
        public String pkg;
        public File file;
        public int version_code;

        public boolean delete() {
            return !(file != null && file.exists()) || file.delete();
        }

        public int getDescription(){
            switch (state){
                case BackupManager.BackupInfo.STATE_BACKUP_IS_NEW:
                    return R.string.backup_state_desc_backup_is_new;
                case BackupManager.BackupInfo.STATE_BACKUPED:
                    return R.string.backup_state_desc_backuped;
                case BackupManager.BackupInfo.STATE_HAS_NEW_VERSION:
                    return R.string.backup_state_desc_has_new_version;
                case BackupManager.BackupInfo.STATE_NOT_BACKUP:
                    return R.string.backup_state_desc_not_backup;
            }
            return 0;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(state + "");
            if (!TextUtils.isEmpty(pkg)) {
                sb.append(", ").append(pkg);
            }
            if (version_code != 0) {
                sb.append(", ").append(version_code);
            }
            if (file != null) {
                sb.append(", ").append(file.getPath());
            }
            return sb.toString();
        }
    }

    public interface BackupListener {
        int RES_SUCCESS = 1;//成功
        int RES_RUNNING = 2;//正在备份
        int RES_FAILED = 3;//失败
        int RES_CANNCELLED = 4;//取消
        int RES_STORAGE_NOT_AVAILABLE = 5;//存储不可用
        int RES_BACKUPED = 6;//已备份

        void onBackupCallback(int code);
    }

    /**
     * 获取备份code描述
     */
    public static int getCodeDescription(int code) {
        int resId = R.string.backup_apk_failed;
        switch (code) {
            case BackupListener.RES_SUCCESS:
                resId = R.string.backup_apk_success;
                break;
            case BackupListener.RES_RUNNING:
                resId = R.string.backup_apk_failed;
                break;
            case BackupListener.RES_FAILED:
                resId = R.string.backup_apk_failed;
                break;
            case BackupListener.RES_CANNCELLED:
                resId = R.string.backup_apk_failed;
                break;
            case BackupListener.RES_STORAGE_NOT_AVAILABLE:
                resId = R.string.backup_apk_failed;
                break;
            case BackupListener.RES_BACKUPED:
                resId = R.string.backup_apk_failed;
                break;
            default:
                break;
        }
        return resId;
    }

    /**
     * 备份应用
     */
    public static void backupApp(AppInfo app, BackupListener listener) {
        BackupTask task = sBackupThreadMap.get(app.getPackageName());
        if (task == null) {
            task = new BackupTask(app, listener);
            sBackupThreadMap.put(app.getPackageName(), task);
            task.start();
        } else {
            if (listener != null) {
                listener.onBackupCallback(BackupListener.RES_RUNNING);
            }
        }
    }

    /**
     * 取消备份
     */
    public static void cancelBackup(AppInfo app) {
        BackupTask task = sBackupThreadMap.get(app.getPackageName());
        if (task != null) {
            task.cancel();
        }
    }

    /**
     * 获取备份信息
     */
    public static BackupInfo getBackupInfo(final AppInfo app) {
        BackupInfo res = null;
        File backupDir = new File(App.getExternalFolder(), BACKUP_FOLDER);
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                if (!TextUtils.isEmpty(filename)) {
                    return filename.contains(app.getPackageName());
                }
                return false;
            }
        };

        //如果有多个备份,保留最新,删除其他
        File[] files = backupDir.listFiles(filter);
        if (files == null || files.length == 0) {
            return new BackupInfo();
        }
        for (File file : files) {
            BackupInfo info = getBackupInfo(file, app);
            if (res == null) {
                res = info;
            } else {
                if (res.version_code >= info.version_code) {
                    info.delete();
                } else {
                    res.delete();
                    res = info;
                }
            }
        }
        return res;
    }

    public static BackupInfo getBackupInfo(File file, AppInfo app) {
        BackupInfo res = new BackupInfo();
        if (!file.exists()) {
            return res;
        }
        String fileName = file.getName();
        fileName = file.getName().substring(0, fileName.lastIndexOf("."));
        String[] items = fileName.split(FILE_NAME_DIVIDER);
        try {
            res = new BackupInfo();
            res.pkg = items[1];
            res.file = file;
            res.version_code = Integer.parseInt(items[2]);
            if (res.version_code == app.getVersionCode()) {
                res.state = BackupInfo.STATE_BACKUPED;
            } else if (res.version_code > app.getVersionCode()) {
                res.state = BackupInfo.STATE_BACKUP_IS_NEW;
            } else if (res.version_code < app.getVersionCode()) {
                res.state = BackupInfo.STATE_HAS_NEW_VERSION;
            }
        } catch (Exception e) {
        }
        return res;
    }

}
