package com.penghaonan.appmanager.t9;

import org.junit.Test;

import java.util.List;

public class T9UtilsTest {

    @Test
    public void str2Pinyin() {
        String pinyin = "微信";
        List<String> outputs = T9Utils.str2Pinyin(pinyin);
        for (String output : outputs) {
            String t9 = T9Utils.str2T9(output);
            TestUtils.log(output);
            TestUtils.log(t9);
        }
    }
}